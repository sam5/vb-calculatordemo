﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.btnC = New System.Windows.Forms.Button()
        Me.btnPlus = New System.Windows.Forms.Button()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.Button4 = New System.Windows.Forms.Button()
        Me.opMultiply = New System.Windows.Forms.Button()
        Me.Button6 = New System.Windows.Forms.Button()
        Me.Button7 = New System.Windows.Forms.Button()
        Me.btn7 = New System.Windows.Forms.Button()
        Me.opMinus = New System.Windows.Forms.Button()
        Me.Button10 = New System.Windows.Forms.Button()
        Me.Btn5 = New System.Windows.Forms.Button()
        Me.btn4 = New System.Windows.Forms.Button()
        Me.opPlus = New System.Windows.Forms.Button()
        Me.btn3 = New System.Windows.Forms.Button()
        Me.btn2 = New System.Windows.Forms.Button()
        Me.btn1 = New System.Windows.Forms.Button()
        Me.btnEquals = New System.Windows.Forms.Button()
        Me.btnCos = New System.Windows.Forms.Button()
        Me.btnPower2 = New System.Windows.Forms.Button()
        Me.btnZero = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'TextBox1
        '
        Me.TextBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox1.Location = New System.Drawing.Point(13, 13)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.ReadOnly = True
        Me.TextBox1.Size = New System.Drawing.Size(166, 38)
        Me.TextBox1.TabIndex = 0
        Me.TextBox1.Text = "0"
        Me.TextBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'btnC
        '
        Me.btnC.Location = New System.Drawing.Point(9, 57)
        Me.btnC.Name = "btnC"
        Me.btnC.Size = New System.Drawing.Size(37, 32)
        Me.btnC.TabIndex = 1
        Me.btnC.Text = "C"
        Me.btnC.UseVisualStyleBackColor = True
        '
        'btnPlus
        '
        Me.btnPlus.Location = New System.Drawing.Point(52, 57)
        Me.btnPlus.Name = "btnPlus"
        Me.btnPlus.Size = New System.Drawing.Size(37, 32)
        Me.btnPlus.TabIndex = 2
        Me.btnPlus.Text = "+/-"
        Me.btnPlus.UseVisualStyleBackColor = True
        '
        'Button3
        '
        Me.Button3.Location = New System.Drawing.Point(95, 57)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(37, 32)
        Me.Button3.TabIndex = 3
        Me.Button3.Text = "Button3"
        Me.Button3.UseVisualStyleBackColor = True
        '
        'Button4
        '
        Me.Button4.Location = New System.Drawing.Point(138, 57)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(37, 32)
        Me.Button4.TabIndex = 4
        Me.Button4.Text = "/"
        Me.Button4.UseVisualStyleBackColor = True
        '
        'opMultiply
        '
        Me.opMultiply.Location = New System.Drawing.Point(138, 95)
        Me.opMultiply.Name = "opMultiply"
        Me.opMultiply.Size = New System.Drawing.Size(37, 32)
        Me.opMultiply.TabIndex = 8
        Me.opMultiply.Text = "x"
        Me.opMultiply.UseVisualStyleBackColor = True
        '
        'Button6
        '
        Me.Button6.Location = New System.Drawing.Point(95, 95)
        Me.Button6.Name = "Button6"
        Me.Button6.Size = New System.Drawing.Size(37, 32)
        Me.Button6.TabIndex = 7
        Me.Button6.Text = "Button6"
        Me.Button6.UseVisualStyleBackColor = True
        '
        'Button7
        '
        Me.Button7.Location = New System.Drawing.Point(52, 95)
        Me.Button7.Name = "Button7"
        Me.Button7.Size = New System.Drawing.Size(37, 32)
        Me.Button7.TabIndex = 6
        Me.Button7.Text = "Button7"
        Me.Button7.UseVisualStyleBackColor = True
        '
        'btn7
        '
        Me.btn7.Location = New System.Drawing.Point(9, 95)
        Me.btn7.Name = "btn7"
        Me.btn7.Size = New System.Drawing.Size(37, 32)
        Me.btn7.TabIndex = 5
        Me.btn7.Text = "7"
        Me.btn7.UseVisualStyleBackColor = True
        '
        'opMinus
        '
        Me.opMinus.Location = New System.Drawing.Point(138, 133)
        Me.opMinus.Name = "opMinus"
        Me.opMinus.Size = New System.Drawing.Size(37, 32)
        Me.opMinus.TabIndex = 12
        Me.opMinus.Text = "-"
        Me.opMinus.UseVisualStyleBackColor = True
        '
        'Button10
        '
        Me.Button10.Location = New System.Drawing.Point(95, 133)
        Me.Button10.Name = "Button10"
        Me.Button10.Size = New System.Drawing.Size(37, 32)
        Me.Button10.TabIndex = 11
        Me.Button10.Text = "Button10"
        Me.Button10.UseVisualStyleBackColor = True
        '
        'Btn5
        '
        Me.Btn5.Location = New System.Drawing.Point(52, 133)
        Me.Btn5.Name = "Btn5"
        Me.Btn5.Size = New System.Drawing.Size(37, 32)
        Me.Btn5.TabIndex = 10
        Me.Btn5.Text = "5"
        Me.Btn5.UseVisualStyleBackColor = True
        '
        'btn4
        '
        Me.btn4.Location = New System.Drawing.Point(9, 133)
        Me.btn4.Name = "btn4"
        Me.btn4.Size = New System.Drawing.Size(37, 32)
        Me.btn4.TabIndex = 9
        Me.btn4.Text = "4"
        Me.btn4.UseVisualStyleBackColor = True
        '
        'opPlus
        '
        Me.opPlus.Location = New System.Drawing.Point(138, 171)
        Me.opPlus.Name = "opPlus"
        Me.opPlus.Size = New System.Drawing.Size(37, 32)
        Me.opPlus.TabIndex = 16
        Me.opPlus.Text = "+"
        Me.opPlus.UseVisualStyleBackColor = True
        '
        'btn3
        '
        Me.btn3.Location = New System.Drawing.Point(95, 171)
        Me.btn3.Name = "btn3"
        Me.btn3.Size = New System.Drawing.Size(37, 32)
        Me.btn3.TabIndex = 15
        Me.btn3.Text = "3"
        Me.btn3.UseVisualStyleBackColor = True
        '
        'btn2
        '
        Me.btn2.Location = New System.Drawing.Point(52, 171)
        Me.btn2.Name = "btn2"
        Me.btn2.Size = New System.Drawing.Size(37, 32)
        Me.btn2.TabIndex = 14
        Me.btn2.Text = "2"
        Me.btn2.UseVisualStyleBackColor = True
        '
        'btn1
        '
        Me.btn1.Location = New System.Drawing.Point(9, 171)
        Me.btn1.Name = "btn1"
        Me.btn1.Size = New System.Drawing.Size(37, 32)
        Me.btn1.TabIndex = 13
        Me.btn1.Text = "1"
        Me.btn1.UseVisualStyleBackColor = True
        '
        'btnEquals
        '
        Me.btnEquals.Location = New System.Drawing.Point(138, 209)
        Me.btnEquals.Name = "btnEquals"
        Me.btnEquals.Size = New System.Drawing.Size(37, 32)
        Me.btnEquals.TabIndex = 20
        Me.btnEquals.Text = "="
        Me.btnEquals.UseVisualStyleBackColor = True
        '
        'btnCos
        '
        Me.btnCos.Location = New System.Drawing.Point(95, 209)
        Me.btnCos.Name = "btnCos"
        Me.btnCos.Size = New System.Drawing.Size(37, 32)
        Me.btnCos.TabIndex = 19
        Me.btnCos.Text = "Cos"
        Me.btnCos.UseVisualStyleBackColor = True
        '
        'btnPower2
        '
        Me.btnPower2.Location = New System.Drawing.Point(52, 209)
        Me.btnPower2.Name = "btnPower2"
        Me.btnPower2.Size = New System.Drawing.Size(37, 32)
        Me.btnPower2.TabIndex = 18
        Me.btnPower2.Text = "^2"
        Me.btnPower2.UseVisualStyleBackColor = True
        '
        'btnZero
        '
        Me.btnZero.Location = New System.Drawing.Point(9, 209)
        Me.btnZero.Name = "btnZero"
        Me.btnZero.Size = New System.Drawing.Size(37, 32)
        Me.btnZero.TabIndex = 17
        Me.btnZero.Text = "0"
        Me.btnZero.UseVisualStyleBackColor = True
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(187, 251)
        Me.Controls.Add(Me.btnEquals)
        Me.Controls.Add(Me.btnCos)
        Me.Controls.Add(Me.btnPower2)
        Me.Controls.Add(Me.btnZero)
        Me.Controls.Add(Me.opPlus)
        Me.Controls.Add(Me.btn3)
        Me.Controls.Add(Me.btn2)
        Me.Controls.Add(Me.btn1)
        Me.Controls.Add(Me.opMinus)
        Me.Controls.Add(Me.Button10)
        Me.Controls.Add(Me.Btn5)
        Me.Controls.Add(Me.btn4)
        Me.Controls.Add(Me.opMultiply)
        Me.Controls.Add(Me.Button6)
        Me.Controls.Add(Me.Button7)
        Me.Controls.Add(Me.btn7)
        Me.Controls.Add(Me.Button4)
        Me.Controls.Add(Me.Button3)
        Me.Controls.Add(Me.btnPlus)
        Me.Controls.Add(Me.btnC)
        Me.Controls.Add(Me.TextBox1)
        Me.Name = "Form1"
        Me.Text = "Form1"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents TextBox1 As TextBox
    Friend WithEvents btnC As Button
    Friend WithEvents btnPlus As Button
    Friend WithEvents Button3 As Button
    Friend WithEvents Button4 As Button
    Friend WithEvents opMultiply As Button
    Friend WithEvents Button6 As Button
    Friend WithEvents Button7 As Button
    Friend WithEvents btn7 As Button
    Friend WithEvents opMinus As Button
    Friend WithEvents Button10 As Button
    Friend WithEvents Btn5 As Button
    Friend WithEvents btn4 As Button
    Friend WithEvents opPlus As Button
    Friend WithEvents btn3 As Button
    Friend WithEvents btn2 As Button
    Friend WithEvents btn1 As Button
    Friend WithEvents btnEquals As Button
    Friend WithEvents btnCos As Button
    Friend WithEvents btnPower2 As Button
    Friend WithEvents btnZero As Button
End Class
